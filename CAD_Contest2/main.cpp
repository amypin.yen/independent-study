#include <iostream>
#include <iostream>
#include <map>
#include <utility>
#include <string>
#include <fstream>
#include <queue>
#include <stack>
#include <math.h>
#include <set>
#include <algorithm>
#include <sstream>
#include <ctime>
using namespace std;
typedef struct state{
    /**** <<input,output>,where it goes to> ****/
    map<string,pair<string,string> > fanout;

    /****If it's empty, that means it's unreachable.****/
    string latency;

    /***Transition number***/
    int fanout_num;

    bool reachable;

}State;

bool there_is_result = false;
bool my_alarm = false;
int total_cost = -10406;

typedef map<string,set<string> >::iterator Mit; // for transition map
typedef map<pair<string,string>,set<string> >::iterator Mit_in;
typedef multimap<pair<string,string>,string>::iterator  Mitfanin; // for fanin transition in state map
typedef map<string,pair<string,string> >::iterator  Mitfanout;

/**** All the states ****/
map<string,State> States;
/****<input,state>    Fanout transition****/
map<string,set<string> > Transitions;
/**** set of state names****/
set<string> State_name;

/**** All the states ****/
map<string,State> original_States;
/****<input,state>    Fanout transition****/
map<string,set<string> > original_Transitions;
/**** set of state names****/
set<string> original_State_name;

/**** All the states ****/
map<string,State> best_States;
/**** <input,state>    Fanout transition ****/
map<string,set<string> > best_Transitions;
/**** set of state names ****/
set<string> best_State_name;

/**** All the states ****/
map<string,State> temp_States;
/****<input,state>    Fanout transition****/
map<string,set<string> > temp_Transitions;
/**** set of state names****/
set<string> temp_State_name;

/****start state****/
string start_state = "";
string start_state1 = "";
string start_state2 = "";
string start_state3= "";
string temp_start_state = "";

string temp_start_state1 = "";
string temp_start_state2 = "";
string temp_start_state3 = "";

int original_state_num = 0;
int original_tran_num = 0;

string first_state = "";
int min_tran_num = 2147483647;
int min_state_num = 222222222;
int input_num = 0;
int output_num = 0;
int tran_num = 0;
int state_num = 0;

int temp_tran_num = 0;
int temp_state_num = 0;

int full_input_num = 0;
int complete_state = 0;
int min_complete_state = 0;

bool last_tx = false;
bool got_result = false;
bool first_unreachable = false;

string watermark1 = "";
string watermark2 = "";
string watermark3 = "";
int w1_size = 0;
int w2_size = 0;
int w3_size = 0;

int cost = 0;
int best_cost = 222222222;

bool first = false;

bool possible_complete = false;
string possible_saving_input = "";
string possible_saving_output = "";
string possible_saving_state = "";
string input_file_name = "";

void Initialize();
void output_kiss();
void finding_latency(string source);
void reset(int i);
void TransitonInsert(int i, string s, string from_state, map<string, State>::iterator set_it);
pair<string,string> go_find_pair(State s, string input);
void starting(string w,int position);
bool going_forward(string w,int position, string current_state);
void round_reset(bool change);
void time_out();

set<string> merging(set<string> input_set);
void merge_transition();

int watermark_num = 0;
bool round_third = false;

int main(/*int argc, char *argv[]*/)
{
    //cout<<clock()<<endl;
    //input_file_name = argv[2];
    //cout<<input_file_name<<endl;
    bool outputed = false;
    first = true;
    Initialize();
    //cout<<complete_state<<" "<<state_num;
    if(complete_state == state_num)
    {
        cout<<"It is CSFSM!!\n";
        exit(1);
    }
    /***123***/
    watermark_num = 1;
    starting(watermark1,0);
    reset(1);

    watermark_num = 2;
    //cout<<"w2 begin\n";
    starting(watermark2,0);
    reset(2);

    round_third = true;
    watermark_num = 3;
    //cout<<"w3 begin\n";
    starting(watermark3,0);
    reset(3);

    first = false;

    bool change = true;

//cout<<"123 Cost : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;

    merge_transition();

    total_cost = tran_num - original_tran_num + state_num - original_state_num;
//cout<<"After merging : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;

    round_reset(change);

    if(clock()/CLOCKS_PER_SEC>= 1200){
        output_kiss();
        outputed = true;
        cout<<"output\n";
        if(clock()/CLOCKS_PER_SEC>= 2700)
            exit(1);
    }

    round_third = false;

    /***132***/
    starting(watermark1,0);
    reset(1);

    if(clock()/CLOCKS_PER_SEC>= 1800 && !outputed){
        output_kiss();
        outputed = true;
    }

    starting(watermark3,0);
    reset(3);

    if(clock()/CLOCKS_PER_SEC>= 1800 && !outputed){
        output_kiss();
        outputed = true;
    }

    round_third = true;
    starting(watermark2,0);
    reset(2);

//cout<<"132 Cost : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;
    merge_transition();
//cout<<"After merging : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;
    if(total_cost > tran_num - original_tran_num + state_num - original_state_num){
        change = true;
        total_cost = tran_num - original_tran_num + state_num - original_state_num;
    }else {
        change = false;
    }
    round_reset(change);

    if(clock()/CLOCKS_PER_SEC>= 1800 && (!outputed||change)){
        output_kiss();
        outputed = true;
    }

    round_third = false;

    /***321***/
    starting(watermark3,0);
    reset(3);

    if(clock()/CLOCKS_PER_SEC>= 1800 && (!outputed||change)){
        output_kiss();
        outputed = true;
    }

    starting(watermark2,0);
    reset(2);

    if(clock()/CLOCKS_PER_SEC>= 1800 && (!outputed||change)){
        cout<<"hi";
        output_kiss();
        outputed = true;
    }

    round_third = true;
    starting(watermark1,0);
    reset(1);
//cout<<"321 Cost : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;
    merge_transition();
//cout<<"After merging : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;
    if(total_cost > tran_num - original_tran_num + state_num - original_state_num){
        change = true;
        total_cost = tran_num - original_tran_num + state_num - original_state_num;
    }else {
        change = false;
    }
    round_reset(change);

    if(clock()/CLOCKS_PER_SEC>= 1800 && (!outputed||change)){
        output_kiss();
        outputed = true;
    }

    round_third = false;

    /***312***/
    starting(watermark3,0);
    reset(3);

    //cout<<"w2 begin\n";
    starting(watermark1,0);
    reset(1);

    //cout<<"w3 begin\n";
    round_third = true;
    starting(watermark2,0);
    reset(2);
//cout<<"312 Cost : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;
    merge_transition();
//cout<<"After merging : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;
    if(total_cost > tran_num - original_tran_num + state_num - original_state_num){
        change = true;
        total_cost = tran_num - original_tran_num + state_num - original_state_num;
    }else {
        change = false;
    }
    round_reset(change);

    round_third = false;

    /***213***/
    starting(watermark2,0);
    reset(2);

    //cout<<"w2 begin\n";
    starting(watermark1,0);
    reset(1);

    //cout<<"w3 begin\n";
    round_third = true;
    starting(watermark3,0);
    reset(3);
//cout<<"213 Cost : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;
    merge_transition();
//cout<<"After merging : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;
    if(total_cost > tran_num - original_tran_num + state_num - original_state_num){
        change = true;
        total_cost = tran_num - original_tran_num + state_num - original_state_num;
    }else {
        change = false;
    }
    round_reset(change);

    round_third = false;

    /***231***/
    starting(watermark2,0);
    reset(2);

    //cout<<"w2 begin\n";
    starting(watermark3,0);
    reset(3);

    //cout<<"w3 begin\n";
    round_third = true;
    starting(watermark1,0);
    reset(1);
//cout<<"231 Cost : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;
    merge_transition();
//cout<<"After merging : "<<tran_num - original_tran_num + state_num - original_state_num<<endl;
    if(total_cost > tran_num - original_tran_num + state_num - original_state_num){
        change = true;
        total_cost = tran_num - original_tran_num + state_num - original_state_num;
    }else {
        change = false;
    }
    round_reset(change);


    if(complete_state == state_num)
        cout<<"hey"<<endl;
    //cout<<"first : "<< start_state1<<"\tlatency : "<<States.find(start_state1)->second.latency<<endl;
    //cout<<"second : "<< start_state2<<"\tlatency : "<<States.find(start_state2)->second.latency<<endl;
    //cout<<"third : "<< start_state3<<"\tlatency : "<<States.find(start_state3)->second.latency<<endl;
    output_kiss();

}

void starting(string w,int position)
{
    string input,output,later_input,later_output;
    input = w.substr(position,input_num);
    output = w.substr(position + input_num,output_num);
    position = position + input_num + output_num; // sub current input output
    later_input = w.substr(position,input_num);
    later_output = w.substr(position+input_num,output_num);

    bool found_one = false;

    /**** First state have input****/
    set<string> first_set;
    first_set = Transitions.find(input)->second;
    pair<string,string> s_pair;
    for(set<string>::iterator it = first_set.begin();it!=first_set.end();++it){
        State current = States.find((*it))->second;
        s_pair = go_find_pair(current, input);

        if(found_one && first && clock()/CLOCKS_PER_SEC>= 400*watermark_num)
            return;
        if(s_pair.first.compare(output) == 0 ){
            temp_start_state = (*it);
            if(going_forward(w,0,(*it))){
                found_one = true;
            }
        }
    }

    /**** First state does not have input****/
    if(!found_one){
        set<string> remain;
        set_difference(State_name.begin(),State_name.end(),first_set.begin(),first_set.end(),inserter(remain,remain.begin()));
        for(set<string>::iterator it = remain.begin();it!=remain.end();++it){
            temp_start_state = (*it);
            if(found_one && first && clock()/CLOCKS_PER_SEC>= 100*watermark_num)
                return;
            if(going_forward(w,0,(*it))){
                found_one = true;
            }
        }
    }

    /****Add new state****/
    if(!found_one){
        stringstream ss;
        ss<<state_num;
        string new_state_name = "S" + ss.str();
        temp_start_state = new_state_name;
        State new_state;
        State_name.insert(new_state_name);
        States.insert(pair<string,State>(new_state_name,new_state));
        state_num++;
        cost++;

        first_unreachable = true;

        going_forward(w,0,new_state_name);

    }
}

bool going_forward(string w,int position, string current_state)
{
    if(clock()/CLOCKS_PER_SEC>3000){
        my_alarm = true;
        //cout<<"alarm\n";
    }
    if(first&&got_result&&clock()/CLOCKS_PER_SEC>= 400*watermark_num){
        cout<<"go back\n";
        return true;
    }


    if(my_alarm && there_is_result)
        time_out();
    /**** check if  reached the end****/
    if(position == w1_size){
        if(cost >= best_cost) return true;
        best_States = States;
        best_Transitions = Transitions;
        best_State_name = State_name;
        min_tran_num = tran_num;
        min_state_num = state_num;
        best_cost = cost;
        min_complete_state = complete_state;
        start_state = temp_start_state;
        //cout<<"get result! cost : "<<cost<<"\n";
        //cout<<"start_state : "<<start_state<<endl;
        got_result = true;
        if(first&&round_third)
            there_is_result = true;
        return true;

    }

    string input,output,later_input,later_output;
    input = w.substr(position,input_num);
    output = w.substr(position + input_num,output_num);
    position = position + input_num + output_num; // sub current input output
    if(position != w1_size){
        later_input = w.substr(position,input_num);
        later_output = w.substr(position+input_num,output_num);
    }else{
        later_input = "";
        later_output = "";
    }
    //cout<<"current state : "<<current_state<<endl;
    //cout<<"position : "<< position << endl;
    //cout<<"current input : "<<input<<endl;
    //cout<<"current output : " << output<<endl;
    bool found_one = false;
    bool avoid_reachable = false;

    /****current has input****/
    State current = States.find(current_state)->second;

    if(!first_unreachable){
        current.reachable = true;
    }

    pair<string,string> s_out = go_find_pair(current, input);
    if(s_out.first.compare(output) == 0){

        if(going_forward(w,position,s_out.second)){
            return true;
        }else {
            return false;
        }
    }else if(s_out.first.compare("") != 0){
        /**** output doesn't match****/
        return false;
    }

    /****current doesn't have input****/
    Mitfanout fanout_insert;
    set<string>::iterator trans_insert;
    pair<string,string> pa;
    set<string> transition_set;

    /****check if the first is reachable and if this is the last transition****/
    if(first_unreachable && position == w1_size && current.reachable != false){
        if(cost +1 >= best_cost&&got_result){
            return true;
        }
         /****current state****/
        pa = make_pair(output,start_state);
        fanout_insert = current.fanout.insert(pair<string,pair<string,string> >(input,pa)).first;
        current.fanout_num++;
        if(current.fanout_num == full_input_num){
            complete_state ++;
        }
        //States.insert(pair<string,State>(current_state,current));
        States[current_state] = current;
        /***Transition map***/
        if(Transitions.find(input) == Transitions.end()){
            set<string> new_set;
            Transitions.insert(pair<string,set<string> >(input,new_set));
        }
        transition_set = Transitions.find(input)->second;
        trans_insert = transition_set.insert(current_state).first;
        //Transitions.insert(pair<string,set<string>>(input,transition_set));
        Transitions[input] = transition_set;

        cost++;
        tran_num++;

        if(going_forward(w,position,start_state)){
            //cout<<current_state<<endl;
            found_one = true;
        }

        cost--;
        tran_num --;
        /****current state****/
        current.fanout.erase(fanout_insert);
        if(current.fanout_num == full_input_num){
            complete_state --;
        }
        current.fanout_num--;
        //States.insert(pair<string,State>(current_state,current));
        States[current_state] = current;
        /***Transition map***/
        transition_set.erase(trans_insert);
        //Transitions.insert(pair<string,set<string>>(input,transition_set));
        Transitions[input] = transition_set;
    }



    /****find those have later input****/
    set<string> having_later = Transitions.find(later_input)->second;
    State later;
    pair<string,string> later_out;
    for(set<string>::iterator it = having_later.begin(); it != having_later.end();++it){
        if(cost +1 >= best_cost&&got_result){
            return true;
        }
        later = States.find((*it))->second;
        later_out = go_find_pair(later,later_input);
        if(later_out.first.compare(later_output) != 0)
            continue;
        /****current state****/
        pa = make_pair(output,(*it));
        fanout_insert = current.fanout.insert(pair<string,pair<string,string> >(input,pa)).first;
        current.fanout_num++;
        if(current.fanout_num == full_input_num){
            complete_state ++;
        }
        //States.insert(pair<string,State>(current_state,current));
        States[current_state] = current;

        /***Transition map***/
        if(Transitions.find(input) == Transitions.end()){
            set<string> new_set;
            Transitions.insert(pair<string,set<string> >(input,new_set));
        }
        transition_set = Transitions.find(input)->second;
        trans_insert = transition_set.insert(current_state).first;
        Transitions[input] = transition_set;
        //Transitions.insert(pair<string,set<string>>(input,transition_set));

        cost++;
        tran_num++;

        if(going_forward(w,position,(*it))){
            found_one = true;
        }

        cost--;
        tran_num --;
        /****current state****/
        current.fanout.erase(fanout_insert);
        if(current.fanout_num == full_input_num){
            complete_state --;
        }
        current.fanout_num--;
        //States.insert(pair<string,State>(current_state,current));
        States[current_state] = current;
        /***Transition map***/
        //if(Transitions.find(input)->second.find(current_state) == Transitions.find(input)->second.end())
           //cout<<"Wrong!!!!!!!\n";
        transition_set.erase(trans_insert);
        //Transitions.insert(pair<string,set<string>>(input,transition_set));
        Transitions[input] = transition_set;
    }


    /****find those do not have later input****/
    set<string> do_not_have_later;
    set_difference(State_name.begin(),State_name.end(),having_later.begin(),having_later.end(),inserter(do_not_have_later,do_not_have_later.begin()));
    for(set<string>::iterator it = do_not_have_later.begin(); it != do_not_have_later.end() ; ++it){
        if(cost +1 >= best_cost&&got_result){
            return true;
        }
        /****current state****/
        pa = make_pair(output,(*it));
        fanout_insert = current.fanout.insert(pair<string,pair<string,string> >(input,pa)).first;
        current.fanout_num++;
        if(current.fanout_num == full_input_num){
            complete_state ++;
        }
        //States.insert(pair<string,State>(current_state,current));
        States[current_state] = current;

        /***Transition map***/
        if(Transitions.find(input) == Transitions.end()){
            set<string> new_set;
            Transitions.insert(pair<string,set<string> >(input,new_set));
        }
        transition_set = Transitions.find(input)->second;
        trans_insert = transition_set.insert(current_state).first;
        Transitions[input] = transition_set;
        //Transitions.insert(pair<string,set<string>>(input,transition_set));

        cost++;
        tran_num++;

        if(going_forward(w,position,(*it))){
            found_one = true;
        }

        cost--;
        tran_num --;
        /****current state****/
        current.fanout.erase(fanout_insert);
        if(current.fanout_num == full_input_num){
            complete_state --;
        }
        current.fanout_num--;
        //States.insert(pair<string,State>(current_state,current));
        States[current_state] = current;

        /***Transition map***/
        transition_set.erase(trans_insert);
        //Transitions.insert(pair<string,set<string>>(input,transition_set));
        Transitions[input] = transition_set;
    }


    /****Still not found anything****/
    if(!found_one){
        if(cost +1 >= best_cost&&got_result){
            return true;
        }
        /****Adding new state****/
        stringstream ss;
        ss<<state_num;
        string new_state_name = "S" + ss.str();
        set<string>::iterator state_name_insert = State_name.insert(new_state_name).first;
        //State_name.insert(new_state_name);
        //if(State_name.find("S10") == State_name.end())
            //cout<<new_state_name<<endl;
        state_num++;
        cost++;

        State new_state;
        map<string,State>::iterator state_insert = States.insert(pair<string,State>(new_state_name,new_state)).first;
        //States.insert(pair<string,State>(new_state_name,new_state));

        /****Adding transition to new state****/
        /****current state****/
        pa = make_pair(output,new_state_name);
        fanout_insert = current.fanout.insert(pair<string,pair<string,string> >(input,pa)).first;
        current.fanout_num++;
        if(current.fanout_num == full_input_num){
            complete_state ++;
        }
        //States.insert(pair<string,State>(current_state,current));
        States[current_state] = current;

        /***Transition map***/
        if(Transitions.find(input) == Transitions.end()){
            set<string> new_set;
            Transitions.insert(pair<string,set<string> >(input,new_set));
        }
        transition_set = Transitions.find(input)->second;
        trans_insert = transition_set.insert(current_state).first;
        //Transitions.insert(pair<string,set<string>>(input,transition_set));
        Transitions[input] = transition_set;

        cost++;
        tran_num++;

        if(going_forward(w,position,new_state_name)){
            found_one = true;
        }

        cost--;
        tran_num--;
        /****current state****/
        current.fanout.erase(fanout_insert);
        if(current.fanout_num == full_input_num){
            complete_state --;
        }
        current.fanout_num--;
        //States.insert(pair<string,State>(current_state,current));
        States[current_state] = current;

        /***Transition map***/
        transition_set.erase(trans_insert);
        //Transitions.insert(pair<string,set<string>>(input,transition_set));
        Transitions[input] = transition_set;

        /***State name and States map***/
        cost --;
        state_num--;

        //map<string,State>::iterator state_insert = States.find(new_state_name);
        States.erase(state_insert);
        //set<string>::iterator state_name_insert = State_name.find(new_state_name);
        State_name.erase(state_name_insert);

    }

    return found_one;
}

void Initialize()
{
        /**** read kiss ****/
    fstream infile(/*input_file_name.c_str()*/"t2.kiss",ios_base::in);
    string str = "";
    infile>>str; // .i
    infile>>input_num; // input_num
    infile>>str; // .o
    infile>>output_num;
    infile>>str; // .p
    infile>>tran_num;
    infile>>str; // .s
    infile>>state_num;
    infile>>str; // .r
    infile>>first_state;

    full_input_num = (int)pow(2,input_num);

    original_state_num = state_num;
    original_tran_num = tran_num;
    string input;
    string output;
    string from_state;
    string to_state;
    map<string,State>::iterator sit;
    Mit it;
    Mit_in it_in;
    pair <string,string> pa;
    //int two = 2;
    for(int i = 0; i<tran_num ; i++){
        infile>>input;
        infile>>from_state;
        infile>>to_state;
        infile>>output;
        /****search for from_state****/
        sit = States.find(from_state);
        pa = make_pair(output,to_state);
        if(sit == States.end()){
            State new_state;
            new_state.reachable = true;
            //new_state.fanout.insert(pair<string,pair<string,string> >(input,pa));
            new_state.fanout_num = 0;
            new_state.latency = "";

            /****if complete****/
            //if(new_state.fanout_num >= full_input_num)
                //complete_state++;
            States.insert(pair<string,State>(from_state,new_state));
            State_name.insert(from_state);

            sit = States.find(from_state);
        }
        //else {
            //sit->second.fanout.insert(pair<string,pair<string,string> >(input,pa));
            //sit->second.fanout_num += (int)pow(two,count(input.begin(),input.end(),'-'));
            /****if complete****/
            //if(sit->second.fanout_num >= full_input_num)
                //complete_state++;
        //}

        if(input.find('-')!=std::string::npos){
            TransitonInsert(0,input,from_state, sit);
        }else{
            it = Transitions.find(input);
            if(it!=Transitions.end()){
                it->second.insert(from_state);
            }else{
                set<string> myset;
                myset.insert(from_state);
                Transitions.insert(pair<string,set<string> >(input,myset));
            }
            if(go_find_pair(sit->second, input).first.compare("") == 0){
              sit->second.fanout_num++;
            }
        }

        sit->second.fanout.insert(pair<string,pair<string,string> >(input,pa));

        if(sit->second.fanout_num == full_input_num){
            complete_state++;
            //cout<<input<<" "<<from_state<<endl;
        }
        if(sit->second.fanout_num > full_input_num){
            cout<<input<<endl;
            //cout<<"Wrong : "<<from_state<<endl;
        }

    }
    infile.close();

    /****save original****/
    original_States = States;
    original_State_name = State_name;
    original_Transitions = Transitions;



    //add 0 if cannot be divided
    fstream w1("md5_1.dat",ios_base::in);
    fstream w2("md5_2.dat",ios_base::in);
    fstream w3("md5_3.dat",ios_base::in);
    char c;
    while(w1>>c){
        switch(c)
        {
            case '0':
                watermark1 = watermark1 + "0000";
                break;
            case '1':
                watermark1 = watermark1 + "0001";
                break;
            case '2':
                watermark1 = watermark1 + "0010";
                break;
            case '3':
                watermark1 = watermark1 + "0011";
                break;
            case '4':
                watermark1 = watermark1 + "0100";
                break;
            case '5':
                watermark1 = watermark1 + "0101";
                break;
            case '6':
                watermark1 = watermark1 + "0110";
                break;
            case '7':
                watermark1 = watermark1 + "0111";
                break;
            case '8':
                watermark1 = watermark1 + "1000";
                break;
            case '9':
                watermark1 = watermark1 + "1001";
                break;
            case 'a':
                watermark1 = watermark1 + "1010";
                break;
            case 'b':
                watermark1 = watermark1 + "1011";
                break;
            case 'c':
                watermark1 = watermark1 + "1100";
                break;
            case 'd':
                watermark1 = watermark1 + "1101";
                break;
            case 'e':
                watermark1 = watermark1 + "1110";
                break;
            case 'f':
                watermark1 = watermark1 + "1111";
                break;
        }
    }
    w1.close();
    while(w2>>c){
        switch(c)
        {
            case '0':
                watermark2 = watermark2 + "0000";
                break;
            case '1':
                watermark2 = watermark2 + "0001";
                break;
            case '2':
                watermark2 = watermark2 + "0010";
                break;
            case '3':
                watermark2 = watermark2 + "0011";
                break;
            case '4':
                watermark2 = watermark2 + "0100";
                break;
            case '5':
                watermark2 = watermark2 + "0101";
                break;
            case '6':
                watermark2 = watermark2 + "0110";
                break;
            case '7':
                watermark2 = watermark2 + "0111";
                break;
            case '8':
                watermark2 = watermark2 + "1000";
                break;
            case '9':
                watermark2 = watermark2 + "1001";
                break;
            case 'a':
                watermark2 = watermark2 + "1010";
                break;
            case 'b':
                watermark2 = watermark2 + "1011";
                break;
            case 'c':
                watermark2 = watermark2 + "1100";
                break;
            case 'd':
                watermark2 = watermark2 + "1101";
                break;
            case 'e':
                watermark2 = watermark2 + "1110";
                break;
            case 'f':
                watermark2 = watermark2 + "1111";
                break;
        }
    }
    w2.close();
    while(w3>>c){
        switch(c)
        {
            case '0':
                watermark3 = watermark3 + "0000";
                break;
            case '1':
                watermark3 = watermark3 + "0001";
                break;
            case '2':
                watermark3 = watermark3 + "0010";
                break;
            case '3':
                watermark3 = watermark3 + "0011";
                break;
            case '4':
                watermark3 = watermark3 + "0100";
                break;
            case '5':
                watermark3 = watermark3 + "0101";
                break;
            case '6':
                watermark3 = watermark3 + "0110";
                break;
            case '7':
                watermark3 = watermark3 + "0111";
                break;
            case '8':
                watermark3 = watermark3 + "1000";
                break;
            case '9':
                watermark3 = watermark3 + "1001";
                break;
            case 'a':
                watermark3 = watermark3 + "1010";
                break;
            case 'b':
                watermark3 = watermark3 + "1011";
                break;
            case 'c':
                watermark3 = watermark3 + "1100";
                break;
            case 'd':
                watermark3 = watermark3 + "1101";
                break;
            case 'e':
                watermark3 = watermark3 + "1110";
                break;
            case 'f':
                watermark3 = watermark3 + "1111";
                break;
        }
    }
    w3.close();
    w1_size = 0;
    for(int i = 1; w1_size<128;i++){
        w1_size = (input_num+output_num)*i;
    }

    if (w1_size!=128){
        for (int i = 128;i<w1_size;i++){
            watermark1 = watermark1 + "0";
            watermark2 = watermark2 + "0";
            watermark3 = watermark3 + "0";
        }
    }
    w1_size = watermark1.size();
    w2_size = watermark2.size();
    w3_size = watermark3.size();

}

void output_kiss()
{
    States = temp_States;
    Transitions = temp_Transitions;
    State_name = temp_State_name;
    start_state1 = temp_start_state1;
    start_state2 = temp_start_state2;
    start_state3 = temp_start_state3;
    state_num = temp_state_num;
    tran_num = temp_tran_num;

    finding_latency(first_state);

    fstream output("ofsm.kiss",ios_base::out);
    output<<".i "<<input_num<<'\n';
    output<<".o "<<output_num<<'\n';
    output<<".p "<<tran_num<<'\n';
    output<<".s "<<state_num<<'\n';
    output<<".r "<<first_state<<'\n';
    for(map <string,State>::iterator it = States.begin(); it!=States.end();++it){
        for(Mitfanout it2 = it->second.fanout.begin(); it2 != it->second.fanout.end(); ++it2){
            output<<it2->first<<" "<<it->first<<" "<<it2->second.second<<" "<<it2->second.first<<'\n';
        }
    }
    output<<".e";
    output.close();

    int num_of_latency = 0;
    string str_num_of_latency = "";
    string latency = "";
    stringstream ss;
    string s = "";

    fstream md1("md5_1.env",ios_base::out);
    fstream m1("md5_1.ini",ios_base::out);
    latency = States.find(start_state1)->second.latency;
    num_of_latency = latency.length()/input_num;
    ss.str("");
    ss<< std::hex<< num_of_latency;
    s = "";
    s = ss.str();
    for(int i = s.length() ; i != 8;i++){
        s = "0"+s;
    }
    md1<<s;
    if(latency.find('-')!=std::string::npos){
        for(int i = 0;i<latency.length();i++){
            if(latency[i] == '-')
                latency[i] = '0';
        }
    }
    m1<<latency;
    md1.close();
    m1.close();

    fstream md2("md5_2.env",ios_base::out);
    fstream m2("md5_2.ini",ios_base::out);
    latency = States.find(start_state2)->second.latency;
    num_of_latency = latency.length()/input_num;
    ss.str("");
    ss<< std::hex<< num_of_latency;
    s = "";
    s = ss.str();
    for(int i = s.length() ; i != 8;i++){
        s = "0"+s;
    }
    md2<<s;
    if(latency.find('-')!=std::string::npos){
        for(int i = 0;i<latency.length();i++){
            if(latency[i] == '-')
                latency[i] = '0';
        }
    }
    m2<<latency;
    md2.close();
    m2.close();

    fstream md3("md5_3.env",ios_base::out);
    fstream m3("md5_3.ini",ios_base::out);
    latency = States.find(start_state3)->second.latency;
    num_of_latency = latency.length()/input_num;
    ss.str("");
    ss<< std::hex<< num_of_latency;
    s = ss.str();
    for(int i = s.length() ; i != 8;i++){
        s = "0"+s;
    }
    md3<<s;
    if(latency.find('-')!=std::string::npos){
        for(int i = 0;i<latency.length();i++){
            if(latency[i] == '-')
                latency[i] = '0';
        }
    }
    m3<<latency;
    md3.close();
    m3.close();

}

void reset(int i)
{
    States = best_States;
    Transitions = best_Transitions;

    State_name = best_State_name;
    tran_num = min_tran_num;
    state_num = min_state_num ;
    complete_state =  min_complete_state;

    /****start state****/

    if(i == 1){
        start_state1 = start_state;
        start_state = "";
        last_tx = false;
    }else if(i == 2){
        start_state2 = start_state;
        start_state = "";
        last_tx = true;
    }else{
        start_state3 = start_state;
        start_state = "";
        //return;
    }

    min_tran_num = 2147483647;
    min_state_num = 222222222;

    possible_complete = false;
    possible_saving_input = "";
    possible_saving_output = "";
    possible_saving_state = "";

    cost = 0;
    best_cost = 222222222;

    got_result = false;



}

void finding_latency(string source)
{
    queue<string> waiting;
    string latency;
    State current;
    State next;
    waiting.push(source);
    bool first = false;
    bool second = false;
    bool third = false;
    while(!waiting.empty()){
        current = States.find(waiting.front())->second;
        waiting.pop();
        for(Mitfanout it = current.fanout.begin(); it!=current.fanout.end();++it){
            next = States.find(it->second.second)->second;

            if(next.latency.compare("") == 0 && it->second.second.compare(first_state) != 0){

                next.latency = current.latency + it->first;
                if(it->second.second.compare(start_state1) == 0)
                    first = true;
                else if (it->second.second.compare(start_state2) == 0)
                    second = true;
                else if (it->second.second.compare(start_state3) == 0)
                    third = true;

                next.reachable = true;
                States[it->second.second] = next;
                if (first && second && third)
                    return;
                //States.insert(pair<string,State>(it->second.second,next));
                waiting.push(it->second.second);
            }
        }
    }
}

void TransitonInsert(int i, string s, string from_state, map<string, State>::iterator set_it)
{
    Mit it;
    if(i == s.length()){
        it = Transitions.find(s);
        if(it!=Transitions.end()){
            it->second.insert(from_state);
        }else{
            set<string> myset;
            myset.insert(from_state);
            Transitions.insert(pair<string,set<string> >(s,myset));
        }
        if(go_find_pair(set_it->second, s).first.compare("") == 0){
            set_it->second.fanout_num++;
        }
        return;
    }else{
        if(s[i] == '-'){
            s[i] = '1';
            TransitonInsert(i+1,s,from_state,set_it);
            s[i] = '0';
            TransitonInsert(i+1,s,from_state,set_it);
        }else{
            TransitonInsert(i+1,s,from_state,set_it);
        }
    }
}

pair<string,string> go_find_pair(State s, string input)
{
    for(Mitfanout it = s.fanout.begin(); it!= s.fanout.end(); ++it){
        string s_input = it->first;
        int i = 0;
        for(i = 0; i< s_input.length(); i++){
            if(s_input[i] == '-'){
                continue;
            }else if(s_input[i] != input[i]){
                break;
            }
        }
        if(i == s_input.length()){
            return it->second;
        }
    }
    pair<string,string> p("","");
    return p;
}

void round_reset(bool change)
{
    if(change){
        temp_States = States;
        temp_Transitions = Transitions;
        temp_State_name = State_name;
        temp_start_state1 = start_state1;
        temp_start_state2 = start_state2;
        temp_start_state3 = start_state3;
        temp_state_num = state_num;
        temp_tran_num = tran_num;
    }
    States = original_States;
    Transitions = original_Transitions;
    State_name = original_State_name;
    state_num = original_state_num;
    tran_num = original_tran_num;

}

void merge_transition()
{
    map<pair<string,string>,set<string> > merge_map;
    pair<string,string> current_pair;
    set<string> current_set;
    map<string,pair<string,string> > fanout_map;
    for(map<string,State>::iterator it = States.begin(); it != States.end(); it++){
        fanout_map.clear();
        fanout_map = it->second.fanout;
        for(Mitfanout itout = fanout_map.begin(); itout!=fanout_map.end();++itout){
            current_pair = itout->second;
            if(merge_map.find(current_pair) == merge_map.end()){
                set<string> new_one;
                new_one.insert(itout->first);
                merge_map.insert(pair<pair<string,string>,set<string> >(current_pair,new_one));
            }else{
                merge_map.find(current_pair)->second.insert(itout->first);
            }
        }
        fanout_map.clear();
        for(map<pair<string,string>,set<string> > ::iterator merge_it = merge_map.begin(); merge_it != merge_map.end(); ++merge_it){
            current_set = merge_it->second;
            if(current_set.size()>1){
                unsigned int cur_size = current_set.size();
                set<string> new_current_set = merging(current_set);
                if(cur_size!=new_current_set.size()) {
                    tran_num -=cur_size-new_current_set.size();
                }
                for(set<string>::iterator set_it = new_current_set.begin(); set_it!=new_current_set.end();set_it++)
                    fanout_map.insert(pair<string,pair<string,string> >((*set_it),merge_it->first));
            }else{
                for(set<string>::iterator set_it = current_set.begin(); set_it!=current_set.end();set_it++)
                    fanout_map.insert(pair<string,pair<string,string> >((*set_it),merge_it->first));
            }
        }

        it->second.fanout.clear();
        it->second.fanout = fanout_map;
        merge_map.clear();
    }

}


set<string> merging(set<string> input_set)
{
    set<string> having;
    set<string> not_having;
    set<string> fianl;
    for(set<string>::iterator it = input_set.begin();it!=input_set.end();it++){
        if((*it).find('-') != std::string::npos){
            having.insert(*it);
        }else{
            not_having.insert(*it);
        }
    }
    int one_flag = -1;
    if(not_having.size()>1){
        bool *used = new bool[not_having.size()];
        for(int i = 0;i<not_having.size();i++)
            used[i] = false;
        set<string>::iterator it2;
        set<string>::iterator it;
        int i = 0;
        for(it = not_having.begin(),i = 0 ;it!=not_having.end();it++, i++){
            //if(used[i])
               // continue;
            string s1 = (*it);
            it2 = it;
            it2++;
            int j = i+1;
            for(;it2!=not_having.end();it2++,j++){
                if(used[j])
                    continue;
                string s2 = (*it2);
                for(int k = 0; k<s1.length();k++){
                    if(s1[k]!=s2[k]){
                        if(one_flag == -1){
                            one_flag = i;
                        }
                        else{
                            one_flag = -1;
                            break;
                        }
                    }
                }
                if(one_flag!=-1){
                    s2[one_flag] = '-';
                    used[i] = true;
                    used[j] = true;
                    having.insert(s2);
                    //tran_num --;
                    one_flag = -1;
                    break;
                }
            }
        }
        for(it = not_having.begin(),i = 0;it!=not_having.end();it++, i++){
            if(used[i]==false)
                fianl.insert(*it);
        }
    }
    else{
        for(set<string>::iterator it = not_having.begin();it!=not_having.end();it++){
            fianl.insert(*it);
        }
    }

    if(having.size()>1){
        set<string>::iterator it;
        set<string>::iterator it2;
        int i=0;
        char c = 'm';
        for(it = having.begin(),i = 0;it!=having.end();it++, i++){
            string s1 = (*it);
            it2 = it;
            it2++;
            int j = i+1;
            for(;it2!=having.end();it2++,j++){
                string s2 = (*it2);
                for(int k = 0; k<s1.length();k++){
                    if(s1[k]!=s2[k]){
                        if(one_flag != -1){
                            one_flag = -1;
                            break;
                        }
                        else if(s1[k] == '-'){
                            if(c == 'm') c = 'l';
                            else if(c == 'r') {
                                c = 'm';
                                break;
                            }
                        }else if(s2[k] == '-'){
                            if(c == 'm') c = 'r';
                            else if(c == 'l'){
                                c = 'm';
                                break;
                            }
                        }
                        else if(one_flag == -1){
                            if(c!='m'){
                                c = 'm';
                                break;
                            }
                            one_flag = k;
                        }
                        else{
                            one_flag = -1;
                            break;
                        }
                    }
                }
                if(one_flag!=-1 || c!='m'){
                    having.erase(it);
                    having.erase(it2);
                    if(c == 'l'){
                        having.insert(s1);
                        c = 'm';
                    }else if(c == 'r'){
                        having.insert(s2);
                        c = 'm';
                    }else{
                        s2[one_flag] = '-';
                        having.insert(s2);
                    }
                    it = having.begin();
                    //tran_num --;
                    one_flag = -1;
                    break;
                }
            }
        }

    }
    for(set<string>::iterator it = having.begin();it!=having.end();it++){
        fianl.insert(*it);
    }

    return fianl;
}

void time_out()
{
    States = best_States;
    Transitions = best_Transitions;

    State_name = best_State_name;
    tran_num = min_tran_num;
    state_num = min_state_num ;
    complete_state =  min_complete_state;
    if(total_cost == -10406){
        total_cost = tran_num - original_tran_num + state_num - original_state_num;
        round_reset(true);
    }
    if(total_cost > tran_num - original_tran_num + state_num - original_state_num&&round_third){
        round_reset(true);
    }else {
        round_reset(false);
    }
    output_kiss();
    exit(0);
}
