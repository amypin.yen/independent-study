#include <iostream>
#include <iostream>
#include <map>
#include <utility>
#include <string>
#include <fstream>
#include <queue>
#include <stack>
#include <math.h>
#include <set>
#include <algorithm>
#include <deque>
#include "definition.h"

void Initialize()
{
        /**** read kiss ****/
    fstream infile("t2.kiss",ios_base::in);
    string str = "";
    infile>>str; // .i
    infile>>input_num; // input_num
    infile>>str; // .o
    infile>>output_num;
    infile>>str; // .p
    infile>>tran_num;
    infile>>str; // .s
    infile>>state_num;
    infile>>str; // .r
    infile>>first_state;

    full_input_num = pow(2,input_num);
    original_state_num = state_num;
    string input;
    string output;
    string from_state;
    string to_state;
    map<string,State>::iterator sit;
    Mit it;
    Mit_in it_in;
    pair <string,string> pa;

    for(int i = 0; i<tran_num ; i++){
        infile>>input;
        infile>>from_state;
        infile>>to_state;
        infile>>output;
        /****search for from_state****/
        sit = States.find(from_state);
        pa = make_pair(output,to_state);
        if(sit == States.end()){
            State new_state;
            new_state.reachable = true;
            new_state.fanout.insert(pair<string,pair<string,string>>(input,pa));
            new_state.fanout_num ++;

            /****if complete****/
            if(new_state.fanout_num == full_input_num)
                complete_state++;
            States.insert(pair<string,State>(from_state,new_state));
            State_name.insert(from_state);
        }
        else {
            sit->second.fanout.insert(pair<string,pair<string,string>>(input,pa));
            sit->second.fanout_num ++;
            /****if complete****/
            if(sit->second.fanout_num == full_input_num)
                complete_state++;
        }



        /****search for to_state****/
        /*
        pa = make_pair(input,output);
        sit = States.find(to_state);
        if(sit == States.end()){
            State new_one;
            new_one.reachable = true;
            new_one.fanin.insert(pair<pair<string,string>,string>(pa,from_state));
            States[to_state] = new_one;
            State_name.insert(to_state);
        }
        else{
            sit->second.fanin.insert(pair<pair<string,string>,string>(pa,from_state));
        }
        */
        it = Transitions.find(input);
        if(it!=Transitions.end()){
            it->second.insert(from_state);
        }else{
            set<string> myset;
            myset.insert(from_state);
            Transitions.insert(pair<string,set<string>>(input,myset));
        }
        /*
        it_in = In_Transitions.find(pa);
        if(it_in != In_Transitions.end()){
            it_in->second.insert(to_state);
        }else{
            set<string> myset2;
            myset2.insert(to_state);
            In_Transitions.insert(pair<pair<string,string>,set<string>>(pa,myset2));
        }*/
    }
    infile.close();

    //add 0 if cannot be divided
    fstream w1("md5_1.dat",ios_base::in);
    fstream w2("md5_2.dat",ios_base::in);
    fstream w3("md5_3.dat",ios_base::in);
    char c;
    while(w1>>c){
        switch(c)
        {
            case '0':
                watermark1 = watermark1 + "0000";
                break;
            case '1':
                watermark1 = watermark1 + "0001";
                break;
            case '2':
                watermark1 = watermark1 + "0010";
                break;
            case '3':
                watermark1 = watermark1 + "0011";
                break;
            case '4':
                watermark1 = watermark1 + "0100";
                break;
            case '5':
                watermark1 = watermark1 + "0101";
                break;
            case '6':
                watermark1 = watermark1 + "0110";
                break;
            case '7':
                watermark1 = watermark1 + "0111";
                break;
            case '8':
                watermark1 = watermark1 + "1000";
                break;
            case '9':
                watermark1 = watermark1 + "1001";
                break;
            case 'a':
                watermark1 = watermark1 + "1010";
                break;
            case 'b':
                watermark1 = watermark1 + "1011";
                break;
            case 'c':
                watermark1 = watermark1 + "1100";
                break;
            case 'd':
                watermark1 = watermark1 + "1101";
                break;
            case 'e':
                watermark1 = watermark1 + "1110";
                break;
            case 'f':
                watermark1 = watermark1 + "1111";
                break;
        }
    }
    w1.close();
    while(w2>>c){
        switch(c)
        {
            case '0':
                watermark2 = watermark2 + "0000";
                break;
            case '1':
                watermark2 = watermark2 + "0001";
                break;
            case '2':
                watermark2 = watermark2 + "0010";
                break;
            case '3':
                watermark2 = watermark2 + "0011";
                break;
            case '4':
                watermark2 = watermark2 + "0100";
                break;
            case '5':
                watermark2 = watermark2 + "0101";
                break;
            case '6':
                watermark2 = watermark2 + "0110";
                break;
            case '7':
                watermark2 = watermark2 + "0111";
                break;
            case '8':
                watermark2 = watermark2 + "1000";
                break;
            case '9':
                watermark2 = watermark2 + "1001";
                break;
            case 'a':
                watermark2 = watermark2 + "1010";
                break;
            case 'b':
                watermark2 = watermark2 + "1011";
                break;
            case 'c':
                watermark2 = watermark2 + "1100";
                break;
            case 'd':
                watermark2 = watermark2 + "1101";
                break;
            case 'e':
                watermark2 = watermark2 + "1110";
                break;
            case 'f':
                watermark2 = watermark2 + "1111";
                break;
        }
    }
    w2.close();
    while(w3>>c){
        switch(c)
        {
            case '0':
                watermark3 = watermark3 + "0000";
                break;
            case '1':
                watermark3 = watermark3 + "0001";
                break;
            case '2':
                watermark3 = watermark3 + "0010";
                break;
            case '3':
                watermark3 = watermark3 + "0011";
                break;
            case '4':
                watermark3 = watermark3 + "0100";
                break;
            case '5':
                watermark3 = watermark3 + "0101";
                break;
            case '6':
                watermark3 = watermark3 + "0110";
                break;
            case '7':
                watermark3 = watermark3 + "0111";
                break;
            case '8':
                watermark3 = watermark3 + "1000";
                break;
            case '9':
                watermark3 = watermark3 + "1001";
                break;
            case 'a':
                watermark3 = watermark3 + "1010";
                break;
            case 'b':
                watermark3 = watermark3 + "1011";
                break;
            case 'c':
                watermark3 = watermark3 + "1100";
                break;
            case 'd':
                watermark3 = watermark3 + "1101";
                break;
            case 'e':
                watermark3 = watermark3 + "1110";
                break;
            case 'f':
                watermark3 = watermark3 + "1111";
                break;
        }
    }
    w3.close();
    int mod = 128%(input_num+output_num);
    if (mod != 0){
        for (int i = 1; i < mod;i++){
            watermark1 = watermark1 + "0";
            watermark2 = watermark2 + "0";
            watermark3 = watermark3 + "0";
        }
    }
    w1_size = watermark1.size();
    w2_size = watermark2.size();
    w3_size = watermark3.size();

}

void output_kiss()
{
    fstream output("output.kiss",ios_base::out);
    output<<".i "<<input_num<<'\n';
    output<<".o "<<output_num<<'\n';
    output<<".s "<<state_num<<'\n';
    output<<".p "<<tran_num<<'\n';
    output<<".r "<<first_state<<'\n';
    for(map <string,State>::iterator it = States.begin(); it!=States.end();++it){
        for(Mitfanout it2 = it->second.fanout.begin(); it2 != it->second.fanout.end(); ++it2)
            output<<it2->first<<" "<<it->first<<" "<<it2->second.second<<" "<<it2->second.first<<'\n';
    }
    output<<".e";
}

void reset(int i)
{
    States = best_States;
    Transitions = best_Transitions;

    State_name = best_State_name;
    tran_num = min_tran_num;
    state_num = min_state_num ;
    complete_state =  min_complete_state;

    /****start state****/

    if(i == 1){
        start_state1 = start_state;
        start_state = "";
        last_tx = false;
    }else if(i == 2){
        start_state2 = start_state;
        start_state = "";
        last_tx = true;
    }else{
        start_state3 = start_state;
        start_state = "";
        return;
    }

    min_tran_num = 2147483647;
    min_state_num = 222222222;

    possible_complete = false;
    possible_saving_input = "";
    possible_saving_output = "";
    possible_saving_state = "";

    cost = 0;
    best_cost = 222222222;

    got_result = false;



}

void finding_latency(string source)
{
    queue<string> waiting;
    string latency;
    State current;
    State next;
    waiting.push(source);
    bool first = false;
    bool second = false;
    bool third = false;
    while(!waiting.empty()){
        current = States.find(waiting.front())->second;
        waiting.pop();
        for(Mitfanout it = current.fanout.begin(); it!=current.fanout.end();++it){
            next = States.find(it->second.second)->second;
            cout<<it->second.second<<endl;
            cout<<next.latency<<endl;
            if(next.latency.compare("") == 0 && it->second.second.compare(first_state) != 0){
                next.latency = current.latency + it->first;
                cout<<next.latency<<endl;
                if(it->second.second.compare(start_state1) == 0)
                    first = true;
                else if (it->second.second.compare(start_state2) == 0)
                    second = true;
                else if (it->second.second.compare(start_state3) == 0)
                    third = true;
                if (first && second && third)
                    return;
                next.reachable = true;
                States[it->second.second] = next;
                //States.insert(pair<string,State>(it->second.second,next));
                waiting.push(it->second.second);
            }
        }
    }
}
