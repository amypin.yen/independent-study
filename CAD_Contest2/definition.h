#ifndef DEFINITION_H_INCLUDED
#define DEFINITION_H_INCLUDED


using namespace std;
typedef struct state{
    /**** <<input,output>,where it goes to> ****/
    map<string,pair<string,string>> fanout;

    /****If it's empty, that means it's unreachable.****/
    string latency = "";

    /***Transition number***/
    int fanout_num = 0;

    bool reachable = false;

}State;

typedef map<string,set<string>>::iterator Mit; // for transition map
typedef map<pair<string,string>,set<string>>::iterator Mit_in;
typedef multimap<pair<string,string>,string>::iterator  Mitfanin; // for fanin transition in state map
typedef map<string,pair<string,string>>::iterator  Mitfanout;

/**** All the states ****/
map<string,State> States;
/****<input,state>    Fanout transition****/
map<string,set<string>> Transitions;
/**** set of state names****/
set<string> State_name;


/**** All the states ****/
map<string,State> best_States;
/**** <input,state>    Fanout transition ****/
map<string,set<string>> best_Transitions;
/**** set of state names ****/
set<string> best_State_name;

/****start state****/
string start_state = "";
string start_state1 = "";
string start_state2 = "";
string start_state3= "";

string first_state = "";
int min_tran_num = 2147483647;
int min_state_num = 222222222;
int input_num = 0;
int output_num = 0;
int tran_num = 0;
int state_num = 0;
int original_state_num = 0;
int full_input_num = 0;
int complete_state = 0;
int min_complete_state = 0;

bool last_tx = false;
bool got_result = false;
bool first_unreachable = false;

string watermark1 = "";
string watermark2 = "";
string watermark3 = "";
int w1_size = 0;
int w2_size = 0;
int w3_size = 0;

int cost = 0;
int best_cost = 222222222;

bool possible_complete = false;
string possible_saving_input = "";
string possible_saving_output = "";
string possible_saving_state = "";

void Initialize();
void output_kiss();
void finding_latency(string source);

void starting(string w,int position);
bool going_forward(string w,int position, string current_state);

#endif // DEFINITION_H_INCLUDED
